obj = [1, 2, 3, 4, 5, 6, 7, 8, 9]
for i in obj:
    print(i)  # printing values in list obj

for i in obj:
    print(i * 2)  # printing multiples of 2 of the values in list obj

for j in range(1, 6):
    print(j)  # printing natural numbers from 1 to 5

summation = 0
for j in range(1, 6):
    summation = summation + j
print(summation)  # printing sum of natural numbers from 1 to 5

print("**********************************")
for k in range(1, 20, 2):
    print(k)  # print  numbers from 1 to 20 with a difference 2(i+2)

print("**********************************")
for k in range(1, 20, 3):
    print(k)  # print  numbers from 1 to 20 with a difference 3(i+3)

print("**********************************")
for m in range(20):
    print(m)  # print numbers from 0 to 19 after skipping first index range
