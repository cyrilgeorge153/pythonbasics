# read the file and store all the values in a list
# reverse the list
# write the list back to the file


with open("test.txt", "r") as reader:
    content = reader.readlines()  # [ant,bat,cat,dart,eat]
    rev = reversed(content)
    with open("test.txt", "w") as writer:
        for i in rev:
            writer.write(i)
