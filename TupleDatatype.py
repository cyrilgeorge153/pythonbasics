listValues = ('cyril', 123.45, False, True, 234)
# list can have multiple values and have difrent data types

print(listValues[-1])
# To print last value in a list

print(len(listValues))
# To print length of a list

print(listValues[0:3])
# To print values between the above indexes
# Tuple is immutable.Cannot update,add or delete a value in a tuple
