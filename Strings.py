str1 = "Cyril George"
str2 = "Kadaplackal"
str3 = "Cyril"
print(str1[0])  # print first character of string
print(str1[0:5])  # print substring 'Cyril' from 'CyrilGeorge'
print(str1 + " " + str2)  # concatenation
print(str3 in str1)  # to check particular string is present in another string.Checking is case sensitive
var = str1.split(" ")  # spliting string with space and will return a list
print(var)
print(var[0])
str4 = " Great "
print(str4.strip())   # To trim or strip trailing or leading white space in a string
print(str4.lstrip())
print(str4.rstrip())
