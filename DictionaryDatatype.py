dictionary={'a':123,"b":'hello', 1:12345,'2': 'qwerty'}

print(dictionary['a'])
print(dictionary['b'])
print(dictionary[1])
print(dictionary['2'])


# creating dictionary at run time and adding values into it
dict={}
dict["firstName"]="Cyril"
dict["lastName"]="George"
dict["designation"]="Test Engineer"
dict["company"]="Katzion"
print(dict)
