# with open("test123.txt", "r") as reader:
#     reader.read()

try:
    with open("test123.txt", "r") as reader:
        reader.read()
except:
    print("Exception in try block so control shifted to except block")  # customised user exception message

try:
    with open("test123.txt", "r") as reader:
        reader.read()
except Exception as e:
    print(e)  # printing exception error given by python itself

try:
    with open("test123.txt", "r") as reader:
        reader.read()
except Exception as e:
    print(e)  # printing exception error given by python itself
finally:
    print("Cleaning up resources")  # finally block will be executed even if try block pass or fail
