# x = 5
# while x > 1:
#     if x != 3:  # if condition added to skip value 3 from printing
#         print(x)
#     x = x - 1  # printing values using while loop
# print("******************************")
#
# y = 10
# while y > 1:
#     if y == 2:
#         break  # break statement added to break while loop when y value becomes 2
#     print(y)
#     y = y - 1
# print("******************************")

z = 10
while z > 1:

    if z == 9:
        z=z-1
        continue
    print(z)  # continue  statement added to skip values 7 and 9
    z = z - 1



