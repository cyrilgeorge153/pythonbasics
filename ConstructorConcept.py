class Calculator:
    num = 200

    def __init__(self, a, b):  # creating default constructor
        self.firstNumber = a
        self.secondNumber = b
        print("I am called automatically when on an object reference obj is created for the class calculator")

    def getData(self):
        print("This is a method in class called calculator")

    def addition(self):
        return self.firstNumber + self.secondNumber + self.num


obj = Calculator(2, 3)

obj.getData()
print(obj.num)
print(obj.addition())
obj1 = Calculator(20, 30)
print(obj1.addition())
