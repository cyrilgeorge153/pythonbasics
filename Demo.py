

print('hello world')

# This is a comment
a = 5
# datatypes are not mentioned explicitly in python as that of java and no need of semicolon
print(a)
b, c, d = 15, 12.34, "Test"
print(b)
print(c)
print(d)

# print("value of b is:"+b)
# 11 th line gives error while printing.
print("{}{}{}".format("value of b is:", "  ", b))  # concatenating different data types
# printing type of variables
print(type(b))
print(type(c))
print(type(d))


