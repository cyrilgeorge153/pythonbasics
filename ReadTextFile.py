file = open("test.txt")
# print(file.read())  # to read and print all the contents in text file

# print(file.read(5))  # to read n number of characters from text file by passing parameter

# print(file.readline()) # to read one single line at a time
# print(file.readline())

# line = file.readline()
# while line != "":   # to print values line by line using while loop
#     print(line)
#     line = file.readline()

line = file.readlines()
for i in line:   # to print values line by line using for loop
    print(i)
file.close()
