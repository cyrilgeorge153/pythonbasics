listValues=['cyril', 123.45, False, True,234]
# list can have multiple values and have difrent data types.It is same as array in java

print(listValues[-1])
# To print last value in a list

print(len(listValues))
# To print length of list

print(listValues[0:3])
# To print values between the above indexes

listValues.insert(1,'George')
# inserted value george in 1st index of the list
print(listValues)

listValues.append('End Value')
# To insert value in the last index of the list
print(listValues)

listValues[0]='CYRIL'
# To update a value in list changed value cyril to CYRIL
print(listValues)

del listValues[2]
# To delete a value in the list.Deleted value 123.45 in the list
print(listValues)